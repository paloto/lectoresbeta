<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('version', 'IndexController@version');

    Route::group(['prefix' => 'relatos'], function(){
        Route::get( '/', 'RelatoController@index');
        Route::post('/', 'RelatoController@store');
    });

});
