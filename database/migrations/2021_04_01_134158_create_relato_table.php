<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('relato', function (Blueprint $table) {
            $table->id();
            $table->string(             'titulo', 256);
            $table->unsignedBigInteger( 'user_id');
            $table->tinyInteger(        'status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('version', function (Blueprint $table) {
            $table->id();
            $table->string(             'name',         64);
            $table->unsignedBigInteger( 'orden')    ->default(1);
            $table->string(             'target_type',  64);
            $table->unsignedBigInteger( 'target_id');
            $table->unsignedBigInteger( 'user_id');
            $table->tinyInteger(        'status')   ->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('texto', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'version_id');
            $table->text(               'texto');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relato');
        Schema::dropIfExists('version');
        Schema::dropIfExists('texto');
    }
}
