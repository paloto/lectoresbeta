<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReaccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaccion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'user_id');
            $table->unsignedBigInteger( 'reaccionable_id');
            $table->string(             'reaccionable_type');
            $table->tinyInteger(        'type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reaccion');
    }
}
