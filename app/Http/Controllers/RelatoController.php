<?php

namespace App\Http\Controllers;

use App\Http\Resources\RelatoResource;
use App\Models\Relato;
use App\Models\Texto;
use App\Models\Version;
use App\Traits\Statusable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RelatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relatos = Relato::query()->paginate();

        $relatos->loadMissing([
            'last_version',
            'last_version.texto',
        ]);

        return RelatoResource::collection($relatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();

        $request->validate([
            'titulo'    => 'required|string|max:256',
            'texto'     => 'required',
        ]);

        $titulo = $request->get('titulo');
        $texto  = $request->get('texto');
        $status = $request->get('status', Statusable::$DRAFT);

        $relato = Relato::create([
            'titulo'    => $titulo,
            'user_id'   => $user->getKey(),
            'status'    => $status,
        ]);

        $version = Version::create([
            'name'  => '1',
            'orden' => 1,
            'target_type'   => Relato::class,
            'target_id'     => $relato->getKey(),
            'user_id'       => $user->getKey(),
            'status'        => $status,
        ]);

        $texto = Texto::create([
            'version_id'    => $version->getKey(),
            'texto'         => $texto,
        ]);

        return RelatoResource::make($relato);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Relato  $relato
     * @return \Illuminate\Http\Response
     */
    public function show(Relato $relato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Relato  $relato
     * @return \Illuminate\Http\Response
     */
    public function edit(Relato $relato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Relato  $relato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Relato $relato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Relato  $relato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Relato $relato)
    {
        //
    }
}
