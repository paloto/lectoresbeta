<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RelatoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'user'  => UserResource::make( $this->whenLoaded( 'user') ),
            'titulo'    => $this->titulo,
            'status'    => $this->status,
            'last_version'  => VersionResource::make( $this->whenLoaded('last_version')),
        ];
    }
}
