<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VersionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'order'         => $this->order,
            'target_type'   => $this->target_type,
            'target_id'     => $this->target_id,
            'user_id'       => $this->user_id,
            'status'        => $this->status,
            'texto'         => $this->whenLoaded( 'texto' ),
        ];
    }
}
