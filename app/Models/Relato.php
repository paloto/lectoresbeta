<?php

namespace App\Models;

use App\Traits\Statusable;
use App\Traits\Versionable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Relato extends Model
{
    use HasFactory, Statusable, Versionable, SoftDeletes;

    protected $table = 'relato';

    protected $fillable = [
        'titulo',
        'user_id',
        'status',
    ];


    public function reacciones()
    {
        return $this->morphMany(Reaccion::class, 'reaccionable');
    }

    public function comentarios()
    {
        return $this->morphMany(Comentario::class, 'comentable');
    }


}
