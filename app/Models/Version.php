<?php

namespace App\Models;

use App\Traits\Statusable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Version extends Model
{
    use HasFactory, SoftDeletes, Statusable;

    protected $table = 'version';

    protected $fillable = [
        'name',
        'orden',
        'target_type',
        'target_id',
        'user_id',
        'texto',
        'status',
    ];

    public function texto()
    {
        return $this->hasOne(Texto::class);
    }

}
