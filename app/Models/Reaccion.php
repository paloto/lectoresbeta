<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reaccion extends Model
{
    use HasFactory;

    const TYPES = [
        1   => [
            'keyname'   => 'like',
            'icon'      => '<i class="fas fa-thumbs-up"></i>',
            ],
        2   => [
            'keyname'   => 'love',
            'icon'      => '<i class="fas fa-heart"></i>',
            ],
        3   => [
            'keyname'   => 'dislike',
            'icon'      => '<i class="fas fa-thumbs-down"></i>',
            ],
        4   => [
            'keyname'   => 'angry',
            'icon'      => '<i class="fas fa-angry"></i>',
            ],
        5   => [
            'keyname'   => 'lol',
            'icon'      => '<i class="fas fa-grin-squint-tears"></i>',
            ],
        6   => [
            'keyname'   => 'cry',
            'icon'      => '<i class="fas fa-grin-squint-tears"></i>',
            ],
    ];



    public function reaccionable()
    {
        return $this->morphTo();
    }

}
