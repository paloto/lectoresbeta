<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    use HasFactory;


    public function reacciones()
    {
        return $this->morphMany(Reaccion::class, 'reaccionable');
    }

    public function comentarios()
    {
        return $this->morphMany(Comentario::class, 'comentable');
    }

}
