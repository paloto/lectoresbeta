<?php

namespace App\Traits;

trait Statusable {

    public static $INVALID  = 'invalid';
    public static $DRAFT    = 'draft';
    public static $ACTIVE   = 'active';

    public static $STATUSES = [
        -1 => 'invalid',
        0  => 'draft',
        1  => 'active',
    ];

    public static function getStatusId($status)
    {
        return array_search($status, self::$STATUSES);
    }

    public function getStatusAttribute()
    {
        return self::$STATUSES[ $this->attributes['status'] ];
    }

    public function setStatusAttribute($value)
    {
        $value = self::getStatusId($value);

        if ($value !== null) {
            $this->attributes['status'] = $value;
        }
    }

    public function scopeStatus($query, $status)
    {
        $query->where('status', array_search($status, self::$STATUSES));
    }

    public function isActive()
    {
        return self::getStatusId($this->status) === 1;
    }

    public function isDraft()
    {
        return self::getStatusId($this->status) === 0;
    }
}
