<?php

namespace App\Traits;

use App\Models\Version;

trait Versionable {

    public function last_version()
    {

        return $this
            ->morphOne(Version::class, 'target')
            ->orderBy('updated_at', 'desc');

    }

}
